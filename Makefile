SERVER_BECOME_PASSWD ?= mandatory## sudo password of the server
LOCAL_HOME ?= /home/matteo## home path of the connecting user
LOCAL_USER ?= matteo## the connecting user
ELASTIC_SEARCH_IP ?= 10.8.0.3## Ip where elastic search will be installed
GIT_TOKEN ?= *****

-include PrivateRules.mak

ANSIBLE_EXTRA_VARS = --extra-vars ansible_become_pass=$(SERVER_BECOME_PASSWD) \
	--extra-vars localhome=$(LOCAL_HOME) \
	--extra-vars localuser=$(LOCAL_USER) \
	--extra-vars slack_api_url=$(SLACK_API_URL) \
	--extra-vars git_token=$(GIT_TOKEN) \
	--extra-vars elasticsearch_host_ip=$(ELASTIC_SEARCH_IP)

.PHONY: help

help:  ## show this help.
	@echo "make targets:"
	@grep -hE '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -hE '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " ?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\#\#/  \#/'

sync_server: ## configure the sync server (prometheus, elk, node-exporter and sync)
	@ansible-playbook -i inventory playbooks/sync_server.yml $(ANSIBLE_EXTRA_VARS) 

node_exporter: ## install the node exporter role on the sync server in the inventory file
	@ansible-playbook -i inventory playbooks/node_exporter.yml $(ANSIBLE_EXTRA_VARS) 

sync: ## deploy the sync role on the sync server in the inventory file
	@ansible-playbook -i inventory playbooks/sync.yml $(ANSIBLE_EXTRA_VARS) 

dashboards: ## deploy the sync role on the sync server in the inventory file
	@ansible-playbook -i inventory playbooks/dashboards.yml $(ANSIBLE_EXTRA_VARS) 

checker: ## Run the checker containers
	@ansible-playbook -i inventory playbooks/checker.yml $(ANSIBLE_EXTRA_VARS) 

idl: ## install the idl and processes (calibration and event) stack on the idl inventory host
	@ansible-playbook -i inventory playbooks/prismadriver.yml $(ANSIBLE_EXTRA_VARS) 

dns: ## install the dns server on the vpn prisma server
	@ansible-playbook -i inventory playbooks/dns.yml $(ANSIBLE_EXTRA_VARS) 

reverseproxy: ## install the reverseproxy server on the vpn prisma server
	@ansible-playbook -i inventory playbooks/reverseproxy.yml $(ANSIBLE_EXTRA_VARS) 

elk: ## install the elk stack on the elk inventory host
	@ansible-playbook -i inventory playbooks/elk.yml $(ANSIBLE_EXTRA_VARS) 

docker_ps: ## docker ps on the 2 PRISMA server nodes
	@ansible -i inventory prisma_host_sync -m shell -a "docker ps"
	@ansible -i inventory prisma_host_vpn -m shell -a "docker ps"

docker_sync_logs: ## docker logs on the 2 PRISMA sync containers
	@ansible -i inventory prisma_host_sync -m shell -a "docker logs sync_fripon --tail 20"
	@ansible -i inventory prisma_host_sync -m shell -a "docker logs sync_prisma --tail 20"

run_a_checker: ## run a checker CHECKERFILE to sync old files
	@ansible -i inventory prisma_host_sync -m shell -a \
	"docker run -it -d -v /prismadata:/prismadata -v /home/matteo:/root \
	prismadriver:0.1.0 sh -c 'cd root && sh /prismadata/checkers/$(CHECKERFILE).sh'"