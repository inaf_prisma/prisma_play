# prisma_play
Ansible playbooks for setting up the server and client PRISMA

### Setup

A `PrivateRules.mak` must exist on the root folder of this repository with the `SERVER_BECOME_PASSWD` 
key set to the password for the access to the server and `SLACK_API_URL` for setting up the alert manager.

## How to use

This repo works entirely with Makefile targets. To get help call `make help`:

``` console
$ make help
make targets:
checker                        Run the checker containers
dns                            install the dns server on the vpn prisma server
elk                            install the elk stack on the elk inventory host
help                           show this help.
idl                            install the idl and processes (calibration and event) stack on the idl inventory host
node_exporter                  install the node exporter role on the sync server in the inventory file
sync                           install the sync fripon role on the sync server in the inventory file
sync_server                    install the sync fripon role on the sync server in the inventory file

make vars (+defaults):
ELASTIC_SEARCH_IP ?            10.8.0.3  # Ip where elastic search will be installed
LOCAL_HOME ?                   /home/matteo  # home path of the connecting user
LOCAL_USER ?                   matteo  # the connecting user
SERVER_BECOME_PASSWD ?         mandatory  # sudo password of the server

```