
prometheus_global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 15s

prometheus_node_metric_relabel_configs: []
prometheus_static_node_metric_relabel_configs: 
  - action: replace
    regex: 10\.8\.0\.8:9100
    replacement: teststation:9100
    source_labels:
    - instance
    target_label: instance
  - action: replace
    regex: 10\.8\.0\.6:9100
    replacement: barolo:9100
    source_labels:
    - instance
    target_label: instance
  - action: replace
    regex: 10\.8\.0\.7:9100
    replacement: racalmuto:9100
    source_labels:
    - instance
    target_label: instance

prometheus_remote_write: []

prometheus_remote_read: []

alerts_inhibit_rules: []

prometheus_alertmanager_config:
  - scheme: http
    static_configs:
      - targets: ["{{ ansible_fqdn | default(ansible_host) | default('localhost') }}:9093"]

prometheus_scrape_configs:
- job_name: "prometheus"
  static_configs:
    - targets:
        - "{{ ansible_fqdn | default(ansible_host) | default('localhost') }}:9090"
- job_name: "prisma"
  static_configs:
    - targets:
        - 10.8.0.3:8000 ## prisma exporter
- job_name: "cadvisor"
  static_configs:
    - targets:
        - 10.8.0.3:9323 ## docker metrics 
        - 10.8.0.3:9324 ## cadvisor metrics
        - 10.8.0.1:9323 ## docker metrics 
        - 10.8.0.1:9324 ## cadvisor metrics
- job_name: "node"
  static_configs:
    - targets:
        - 10.8.0.1:9100 ## prismavpn node-exporter
        - 10.8.0.3:9100 ## new server node-exporter
        - 10.8.0.21:9100 ## CODOGNO
        - 10.8.0.22:9100 ## SESTOSANGIOVANNI
        - 10.8.0.25:9100 ## SANMARTINOINSTRADA
        #- 10.8.0.8:9100 ## teststation
        #- 10.8.0.6:9100 ## barolo not working anymore -- lost
        #- 10.8.0.7:9100 ## racalmuto

prometheus_record_rules:
  - record: instance:node_cpu:load
    expr: 100 - (avg by(instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100)

  - record: instance:node_ram:usage
    expr: (1 - node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes) * 100

  - record: instance:node_fs:disk_space
    expr: node_filesystem_avail_bytes{mountpoint="/",job="node"} / node_filesystem_size_bytes{mountpoint="/",job="node"} * 100

prometheus_alert_rules:
- alert: Watchdog
  expr: vector(1)
  for: 10m
  labels:
    severity: warning
  annotations:
    description: 'This is an alert meant to ensure that the entire alerting pipeline is functional.
      This alert is always firing, therefore it should always be firing in Alertmanager
      and always fire against a receiver. There are integrations with various notification
      mechanisms that send a notification when this alert is not firing. For example the
      "DeadMansSnitch" integration in PagerDuty.'
    summary: 'Ensure entire alerting pipeline is functional'
- alert: InstanceDown
  expr: "up == 0"
  for: 5m
  labels:
    severity: critical
  annotations:
    description: "{% raw %}{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes.{% endraw %}"
    summary: "{% raw %}Instance {{ $labels.instance }} down{% endraw %}"
- alert: CriticalCPULoad
  expr: 'instance:node_cpu:load > 98'
  for: 30m
  labels:
    severity: critical
  annotations:
    description: "{% raw %}{{ $labels.instance }} of job {{ $labels.job }} has Critical CPU load for more than 30 minutes.{% endraw %}"
    summary: "{% raw %}Instance {{ $labels.instance }} - Critical CPU load{% endraw %}"
- alert: CriticalRAMUsage
  expr: 'instance:node_ram:usage > 98'
  for: 5m
  labels:
    severity: critical
  annotations:
    description: "{% raw %}{{ $labels.instance }} has Critical Memory Usage more than 5 minutes.{% endraw %}"
    summary: "{% raw %}Instance {{ $labels.instance }} has Critical Memory Usage{% endraw %}"
- alert: CriticalDiskSpace
  expr: 'instance:node_fs:disk_space < 10'
  for: 4m
  labels:
    severity: critical
  annotations:
    description: "{% raw %}{{ $labels.instance }} of job {{ $labels.job }} has less than 10% space remaining.{% endraw %}"
    summary: "{% raw %}Instance {{ $labels.instance }} - Critical disk space usage{% endraw %}"
- alert: ClockSkewDetected
  expr: 'abs(node_timex_offset_seconds) * 1000 > 100'
  for: 2m
  labels:
    severity: warning
  annotations:
    description: "{% raw %}Clock skew detected on {{ $labels.instance }}. Ensure NTP is configured correctly on this host.{% endraw %}"
    summary: "{% raw %}Instance {{ $labels.instance }} - Clock skew detected{% endraw %}"
- alert: CaptureNotAvailable
  expr: 'device_captures == 0'
  for: 2m
  labels:
    severity: critical
  annotations:
    description: "{% raw %} La camera {{ $labels.code }} ({{ $labels.name }}) per il giorno {{ $labels.day }}-{{ $labels.month }}-{{ $labels.year }} non ha captures.{% endraw %}"
    summary: "{% raw %}Camera {{ $labels.code }} - {{ $labels.name }} low captures{% endraw %}"
- alert: CameraNoDetections
  expr: 'device_detection == 0'
  for: 2m
  labels:
    severity: critical
  annotations:
    description: "{% raw %} La camera con codice {{ $labels.code }} ({{ $labels.name }}) negli ultimi 30 giorni non ha avuto alcuna detection. {% endraw %}"
    summary: "{% raw %}Camera {{ $labels.code }} - {{ $labels.name }} low detections{% endraw %}"
- alert: SyncNotWorking
  expr: 'sync_working < 35'
  for: 2m
  labels:
    severity: critical
  annotations:
    description: "{% raw %} La sincronizzazione non sembra essere attiva. {% endraw %}"
    summary: "{% raw %} Nella giornata di ieri non risultano capture. E' possibile quindi che la sincronizzazione non stia funzionando, contattare l'amministratore. {% endraw %}"