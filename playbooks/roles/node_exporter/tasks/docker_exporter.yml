---
- name: Check if docker prometheus exporter is already set
  wait_for:
    port: 9323
    timeout: 3
  register: check_port
  ignore_errors: true

- name: activate metrics
  block:

    - name: Check that the daemon.json exists
      stat:
        path: /etc/docker/daemon.json
      register: stat_result

    - name: Create daemon.json file
      become: yes
      copy:
        dest: /etc/docker/daemon.json
        content: |
          {
            "metrics-addr" : "0.0.0.0:9323",
            "experimental" : true
            "log-driver": "json-file",
            "log-opts": {
                "max-size": "10m",
                "max-file": "5",
            }
          }
        mode: "0644"
      when: not stat_result.stat.exists

    - name: Load daemon.json from file
      slurp:
        src: /etc/docker/daemon.json
      register: daemon

    - name: Load daemon.json
      set_fact:
        daemon: "{{ daemon.content | b64decode | from_json }}"

    - name: debug daemon
      debug:
        var: daemon

    - name: Configure metrics-addr
      set_fact:
        daemon: "{{ daemon | default([]) | combine({ 'metrics-addr': '0.0.0.0:9323' }) }}"
      when: daemon['metrics-addr'] is not defined

    - name: Configure experimental
      set_fact:
        daemon: "{{ daemon | default([]) | combine({ 'experimental': true }) }}"
      when: daemon['experimental'] is not defined

    - name: Get daemon
      debug:
        var: daemon

    - name: Write daemon.json
      become: yes
      copy:
        content: "{{ daemon | to_nice_json }}"
        dest: /etc/docker/daemon.json
        mode: "0644"

    - name: restart and docker
      become: yes
      systemd:
        name: docker
        state: restarted

  when: check_port.failed

- name: Check if cadvisor exporter is already set
  wait_for:
    port: 9324
    timeout: 3
  register: check_port_cadvisor
  ignore_errors: true

- name: activate cadvisor metrics
  block:
    - name: Set a fact containing the original python interpreter
      set_fact:
        old_python_interpreter: "{{ ansible_python_interpreter | default('python3') }}"

    - name: Set a fact to use the python3 interpreter
      set_fact:
        ansible_python_interpreter: "python3"

    - name: Pull cadvisor image {{ cadvisor_docker_tags }}
      docker_image:
        name: gcr.io/cadvisor/cadvisor
        tag: "{{ cadvisor_docker_tags }}"
        source: pull

    - name: Stop cadvisor container
      docker_container:
        name: cadvisor
        state: absent
        force_kill: yes

    - name: volumes for cadvisor
      set_fact:
        cadvisor_vols: "{{ ['/:/rootfs:ro', '/sys:/sys:ro', '/var/lib/docker/:/var/lib/docker:ro',
          '/dev/disk/:/dev/disk:ro'] + ([] if ansible_distribution == 'CentOS' else ['/var/run:/var/run:ro']) }}"

    - name: Start cadvisor container
      docker_container:
        name: cadvisor
        image: "gcr.io/cadvisor/cadvisor:{{ cadvisor_docker_tags }}"
        command:
          - /usr/bin/cadvisor
          - -logtostderr
          - --docker_only
          - --disable_metrics=disk,udp,percpu
        state: started
        user: root
        restart: true
        restart_policy: always
        memory: 256M
        ports:
          - "9324:8080"
        volumes: "{{ cadvisor_vols }}"


    - name: Reset the python interpreter
      set_fact:
        ansible_python_interpreter: "{{ old_python_interpreter }}"

  # when: check_port_cadvisor.failed
